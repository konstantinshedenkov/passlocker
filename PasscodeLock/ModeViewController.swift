//
//  ModeViewController.swift
//  PasscodeLock
//
//  Created by Konstantin Shendenkov on 20.04.17.
//  Copyright © 2017 Konstantin Shendenkov. All rights reserved.
//

import UIKit

class ModeViewController: UIViewController {

  @IBAction func createMode(_ sender: UIButton) {
    pin(.create)
  }
  
  @IBAction func changeMode(_ sender: UIButton) {
    pin(.change)
  }
  
  @IBAction func deactiveMode(_ sender: UIButton) {
    pin(.deactive)
  }
  
  @IBAction func validateMode(_ sender: UIButton) {
    pin(.validate)
  }
  
  func pin(_ mode: LockerMode) {
    
    var appearance = LockerAppearance()
    if let image = UIImage(named: "face") {
      appearance.image = image
    }
    appearance.title = "Pass locker"
    appearance.isSensorsEnabled = true
    
    PassLocker.present(with: mode, and: appearance)
  }

}

