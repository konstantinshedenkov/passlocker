//
//  AppDelegate.swift
//  PasscodeLock
//
//  Created by Konstantin Shendenkov on 19.04.17.
//  Copyright © 2017 Konstantin Shendenkov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    return true
  }

}

